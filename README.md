# Pendulum Associates Website
A statically generated website for Pendulum Associates.


## Getting Started

### Installtion
To get started, install the project dependencies.

````
$ npm install
````

### Build
To build the project, run the NPM build command from the root of the project. The built files will output to the *public/* directory.

````
$ npm run build
````

### Develop
To start the development environment, run the NPM dev command from the root of the project.

````
$ npm run dev
````

### Deploy
This project is set up to be deployed to [Netlify](https://www.netlify.com/), and has a *netlify.toml* configuration file.


## Technologies Used

### Eleventy
[11ty.dev](https://www.11ty.dev/)

### Tailwind CSS
[tailwindcss.com](https://tailwindcss.com/)

### Sass
[sass-lang.com](https://sass-lang.com/)

### Google Fonts
[fonts.google.com](https://fonts.google.com/)

### Font Awesome
[fontawesome.com](https://fontawesome.com/)


