module.exports = {
    purge: [
        './_site/**/*.html',
    ],    
    theme: {
        container: {
            center: true,
            padding: '1rem',
        },
        colors: {
            'red': '#C53030',
            'green': '#48BB78',
            'blue': {
                'default': '#00677F',
                //'100': '#8FBCC7',
                '200': '#63dffb',
                '300': '#47b6d0',
                //'400': '#247C91',
                '500': '#00677F',
                '600': '#025164', //'#00596D',
                '700': '#004A5B',
                //'800': '#003C4A',
                '900': '#002D38',            
            },
            'yellow': {
                'default': '#F1C400',
            },
            'gray': {
                'default': '#999999',
                '100': '#FAFAFA',
                '200': '#E2E2E2',
                '300': '#C9C9C9',
                '400': '#B1B1B1',
                '500': '#999999',
                '600': '#757575',
                '700': '#505050',
                '800': '#2C2C2C',
                '900': '#080808',
            },
            'white': '#ffffff',
            'black': '#000000',          
        }, 
        fontSize: {
            'xs': '.75rem',
            'sm': '.875rem',
            'base': '1rem',
            'lg': '1.125rem',
            'xl': '1.25rem',
            '2xl': '1.5rem',
            '3xl': '1.875rem',
            '4xl': '2.25rem',
            '5xl': '3rem',
            '6xl': '4rem',
        },        
        screens: {
            'sm': '40rem', //40rem - 2rem padding = 38rem = 608px (@16px/rem)
            'md': '58rem', //56rem - 2rem padding = 54rem = 864px (@16px/rem)
            'lg': '70rem', //70rem - 2rem padding = 68rem = 1120px (@16px/rem)
        },                   
        extend: {
            fontFamily: {
                body: ['Roboto', 'sans-serif']
            },      
        },
    },
    variants: {},
    plugins: [],
}