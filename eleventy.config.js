
module.exports = eleventyConfig => {

    eleventyConfig.addPassthroughCopy({ '_site/_images': 'assets/images' });
    eleventyConfig.addLayoutAlias('default', 'default.html');
    eleventyConfig.addFilter("addVersion", url => {
        const now = new Date();
        return url+'?ver='+now.getTime();
    });

    return {
        dir: {
            input: '_site',
            output: 'public',
            layouts: '_layouts',
        },
    }
};